import pandas as pd
from scipy.spatial import distance
from sklearn.neighbors import KNeighborsRegressor
from sklearn.metrics import mean_squared_error

import pandas
from keras.models import Sequential
from keras.layers import Dense
from keras.wrappers.scikit_learn import KerasClassifier
from keras.utils import np_utils
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn.preprocessing import LabelEncoder

norm_train_df = pd.read_csv("dc_train.csv", usecols=[1,2,3,4,5])


dataset = norm_train_df.values
X = dataset[:,0:4]
Y = dataset[:,4]
encoder = LabelEncoder()
print(X)
# print(Y.head())
encoder.fit(Y)
encoded_Y = encoder.transform(Y)
dummy_y = np_utils.to_categorical(encoded_Y)

# define baseline model
def baseline_model():
	# create model
	model = Sequential()
	model.add(Dense(300, input_dim=4, activation='relu'))
	# model.add(Dense(18, activation='softmax'))
	model.add(Dense(268, activation='softmax'))
	# Compile model
	model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
	return model

estimator = KerasClassifier(build_fn=baseline_model, epochs=50, batch_size=5, verbose=0)
kfold = KFold(n_splits=10, shuffle=False)
results = cross_val_score(estimator, X, dummy_y, cv=kfold)
print("Baseline: %.2f%% (%.2f%%)" % (results.mean()*100, results.std()*100))


#Baseline: 4.91% (1.01%)
#
#
#
#
#
#
# norm_test_df = pd.read_csv("dc_norm_test.csv")
#
#
# def predict_price_multivariate(new_listing_value,feature_columns):
#     temp_df = norm_train_df
#     temp_df['distance'] = distance.cdist(temp_df[feature_columns],[new_listing_value[feature_columns]])
#     temp_df = temp_df.sort_values('distance')
#     knn_5 = temp_df.price.iloc[:5]
#     predicted_price = knn_5.mean()
#     return(predicted_price)
#
#
# knn = KNeighborsRegressor(algorithm='brute')
# cols = ['accommodates','bedrooms','bathrooms','number_of_reviews']
# knn.fit(norm_train_df[cols], norm_train_df['price'])
# four_features_predictions = knn.predict(norm_test_df[cols])
# four_features_mse = mean_squared_error(norm_test_df['price'], four_features_predictions)
# four_features_rmse = four_features_mse ** (1/2)
# print("4 features fmse",four_features_rmse)
