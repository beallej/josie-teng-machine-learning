import pandas as pd
from sklearn import preprocessing

import tensorflow as tf
import keras
from keras import layers
import tensorflow_docs as tfdocs
import tensorflow_docs.plots
import tensorflow_docs.modeling
import matplotlib.pyplot as plt



def normalize_listings(listings):
    paris_listings = listings.dropna(axis=0, how='any',
                                           subset=['accommodates', 'bedrooms', 'bathrooms', 'number_of_reviews',
                                                   'price'])

    paris_listings = paris_listings[['accommodates', 'bedrooms', 'bathrooms', 'number_of_reviews', 'price']]

    normalized_listings = preprocessing.normalize(paris_listings)
    normalized_listings = pd.DataFrame(normalized_listings, columns=paris_listings.columns)
    normalized_listings.to_csv('airbnb_paris_normalized.csv')
    normalized_listings = pd.read_csv('airbnb_paris_normalized.csv')
    return normalized_listings

def read_all_data():
    paris_listings = pd.read_csv('listings-paris.csv')
    return paris_listings

def generate_train_test(listings):

    listings = listings.dropna(axis=0, how='any', subset=['accommodates', 'bedrooms', 'bathrooms', 'number_of_reviews', 'price'])

    listings = listings[['accommodates', 'bedrooms', 'bathrooms', 'number_of_reviews', 'price']]
    listings['price'] = listings.price.str.replace("\$|,",'').astype(float)
    listings.price = listings.price.astype(int)
    listings['bedrooms'] = pd.to_numeric(listings['bedrooms'])
    listings.bedrooms = listings.bedrooms.astype(int)
    listings['bathrooms'] = pd.to_numeric(listings['bathrooms'])
    listings.bathrooms = listings.bathrooms.astype(int)

    train_df = listings.sample(frac=0.8, random_state=0)
    test_df = listings.drop(train_df.index)
    train_df.to_csv("paris_train.csv")
    test_df.to_csv("paris_test.csv")

    return train_df, test_df



def build_model(train_dataset):
  model = keras.Sequential([
    layers.Dense(64, activation='relu', input_shape=[len(train_dataset.keys())]),
    layers.Dense(64, activation='relu'),
    layers.Dense(1)
  ])

  optimizer = tf.keras.optimizers.RMSprop(0.001)

  model.compile(loss='mse',
                optimizer=optimizer,
                metrics=['mae', 'mse'])
  return model

def normalize_data(train_df, test_df):
    train_stats = train_df.describe()
    train_stats.pop("price")
    train_stats = train_stats.transpose()

    train_df_labels = train_df.pop('price')
    test_df_labels = test_df.pop('price')

    def norm(x):
        return (x - train_stats['mean']) / train_stats['std']

    normed_train_data = norm(train_df)
    normed_test_data = norm(test_df)

    normed_train_data.to_csv("paris_train_norm.csv")
    normed_test_data.to_csv("paris_test_norm.csv")
    return normed_train_data, normed_test_data, train_df_labels, test_df_labels
def main():
    
    print(tf.keras.__version__)
    listings = read_all_data()
    train_df, test_df = generate_train_test(listings)
    train_norm, test_norm, train_df_labels, test_df_labels = normalize_data(train_df, test_df)

    model = build_model(train_norm)
    example_batch = train_norm[:10]
    example_result = model.predict(example_batch)

    EPOCHS = 100

    # The patience parameter is the amount of epochs to check for improvement
    early_stop = keras.callbacks.EarlyStopping(monitor='val_loss', patience=10)

    history = model.fit(train_norm, train_df_labels,
                              epochs=EPOCHS, validation_split=0.2, verbose=0,
                              callbacks=[early_stop, tfdocs.modeling.EpochDots()])

    plotter = tfdocs.plots.HistoryPlotter(smoothing_std=2)
    plotter.plot({'Basic': history}, metric="mae")
    plt.ylim([0, 10])
    plt.ylabel('MAE [MPG]')

    plotter.plot({'Basic': history}, metric="mse")
    plt.ylim([0, 20])
    plt.ylabel('MSE [MPG^2]')

    loss, mae, mse = model.evaluate(test_norm, test_df_labels, verbose=2)

    print("Testing set Mean Abs Error: {:5.2f} MPG".format(mae))

    test_predictions = model.predict(test_norm).flatten()

    a = plt.axes(aspect='equal')
    plt.scatter(test_df_labels, test_predictions)
    plt.xlabel('True Values [Price]')
    plt.ylabel('Predictions [Price]')
    lims = [0, 1000]
    plt.xlim(lims)
    plt.ylim(lims)
    _ = plt.plot(lims, lims)
    plt.show()
    error = test_predictions - test_df_labels
    plt.hist(error, bins=25)
    plt.xlabel("Prediction Error [Price]")
    _ = plt.ylabel("Count")
    plt.show()

if __name__ == "__main__":
    main()